.PHONY: build clean test test-unit
.DEFAULT_GOAL := build
SHELL := /bin/bash
ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

CLI_DIR := $(ROOT_DIR)/cli
LIB_DIR := $(ROOT_DIR)/lib


# TOOLBOX

toolbox-build:
	cd toolbox && \
	buildah bud -t localhost/townhopper-toolbox:latest . && \
	podman tag localhost/townhopper-toolbox:latest localhost/townhopper-toolbox:$$(date "+%Y_%m_%d")

toolbox-clean:
	podman rmi localhost/czech-timetable-toolbox:latest


# BUILD

BUILD_FLAGS ?= --release

build:
	cd $(LIB_DIR) && cargo build $(BUILD_FLAGS) && \
	cd $(CLI_DIR) && cargo build $(BUILD_FLAGS)

clean:
	cd $(LIB_DIR) && cargo clean && \
	cd $(CLI_DIR) && cargo clean

check-outdated:
	cd $(LIB_DIR) && cargo outdated && \
	cd $(CLI_DIR) && cargo outdated

doc:
	cd $(CLI_DIR) && cargo doc

# TEST

test: test-unit test-func
	echo "ALL TESTS OK"

test-unit:
	cd $(LIB_DIR) && cargo test && \
	cd $(CLI_DIR) && cargo test && \
	echo "UNIT TESTS OK"

# FIXME: The func test rewrites user's data in
# ~/.cache/townhopper. It should use different XDG dirs.
# FIXME: The pushd to CLI_DIR for running func tests is a workaround
# for bad handling of gtfs_sources dir_of_dirs path and can be removed
# once that is fixed.
test-func:
	pushd $(CLI_DIR) && cargo build && popd && \
	pushd $(CLI_DIR) && \
	bash $(CLI_DIR)/test-func/run.sh && \
	popd && \
	echo "FUNC TESTS OK"


# PUBLISH

publish-lib:
	cd $(LIB_DIR) && cargo package && \
	cd $(LIB_DIR) && cargo publish

publish-cli:
	cd $(CLI_DIR) && cargo package && \
	cd $(CLI_DIR) && cargo publish
