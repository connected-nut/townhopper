townhopper
==========

Public transport timetable query application, working with GTFS
timetable format.

Basic principles
----------------

* **GTFS files as single source of truth**. Internal database,
  whatever it may be (maybe SQLite for simplicity?) will essentially
  be just a more easily queriable cache of the GTFS text files.

  This makes data migrations a non-issue, it should be possible to
  delete the DB anytime and recreate it from the GTFS files.

* **Start basic**. (Maybe finish there too? :) ) Find connections
  between stop A and stop B. At least initially there will be no
  support for automatic computation of transfers, users will have to
  specify transfer stops themselves.

* **Library + CLI**. Having the core functionality in a Rust library,
  not very tightly coupled to CLI, should not be very difficult to
  achieve, but it might ease possible future morphing into a
  client/server paradigm, in case it's worth the effort at some point
  in the future.

* Possible bonus goals

  * Stop timetables -- find all departures or arrivals at a
    stop. Optionally specify a 2nd stop which the vehicles must be
    coming from/to. Optionally specify vehicles on a particuar route.

  * Transfer computations -- single transfers might be still doable on
    top of SQLite, not sure about multiple transfers though.

CLI design
----------

### `trip` subcommand

-d TIMESPEC | --departure-time TIMESPEC -- specify departure
time. (TIMESPEC format TBD, maybe use a library.)

-w MINUTES | --wait MINUTES -- reserve gap of at least TIME minutes at
specified point. Order matters, must be placed between -s parameters
where the transfer wait time should apply. If specified between first
two -s params or before the first -s param, it specifies minimal gap
between now and boarding the first vehicle ("find me the first tram at
least 10 mins from now").

-W MINUTES | --global-wait MINUTES -- each transfer will require a gap
of at least TIME minutes. Can be overriden by -t at individual
transfers.

-s STOP | --stop STOP -- specify a stop that the trip needs to pass
through. Can be origin of the trip, transfer or destination stop.

-f STOP | --from STOP -- specify a stop that is used as an origin for
the next part of the trip. Useful when a transfer between vehicles
involves moving to a different stop.

-P PREFIX | --stop-prefix PREFIX -- specify a global prefix that will
be prepended to the name of each stop. Prefixes can be useful when
travelling within a single town. In case the prefixed match is not
found, a literal match is tried subsequently.

### `search stop` subcommand

Look for a stop in the stops database, print all stops which contain
the string provided. In the first iteration, only benevolent matching
by slug is implemented (query is sluggified too before searching).

CLI examples
------------

Find trip from 'Brno, Červinkova' through 'Brno, Česká' to 'Brno,
Mendlovo náměstí', with minimal transfer time of 3 minutes at Česká.

townhopper trip \
    -W 3 \
    -P 'Brno, ' \
    -s Červinkova
    -s Česká \
    -s 'Mendlovo náměstí' \

Same query as before but utilizing automatic conversion to ascii and
spaces to dashes for faster typing, and specifying the transfer time

townhopper trip \
    -P brno- \
    -s cervinkova
    -s ceska \
    -w 3 \
    -s mendlovo-namesti \

Known limitations
-----------------

* All agencies imported into a single cache must have the same
  agency_timezone. This is so that we can trivially compare times in
  the database.

  (Timetables are specified in agency's local time. When combined with
  DST changes, this makes fully generic handling of timezones
  difficult, as the stop times offset from UTC can change throughout
  the year, so converting all time tables to UTC is not
  trivial. Proper handling of this would be to chunk up the date
  service intervals from `calendar.txt` during importing, so that one
  service chunk never crosses DST change boundary, and duplicate
  `stop_times.txt` info for all service chunks, which allows applying
  different UTC offsets for each chunk. Plus make sure that entries in
  `calendar_dates.txt` refer to the correct service chunks too. So
  converting all timetables to UTC should be possible, but it's quite
  a bit of work, and for my personal use i don't currently need the
  feature.)

Code strucutre
--------------

Lib:

* townhopper::db - working with SQLite DB

* townhopper::db::import - crunch GTFS structs into DB entries

* townhopper::gtfs - parser and structs for GTFS

* townhopper::model - structs representing concepts found across the
  app (trip queries, trips, stops etc.)

* townhopper::query::trip - trip planner queries (works with DB,
  produces model structs)

* townhopper::query::stop - stop lookup queries (works with DB,
  produces model structs)

CLI:

* townhopper_cli::trip - trip subcommand
