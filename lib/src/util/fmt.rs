use chrono::NaiveDateTime;

pub fn datetime_hm(dt: &NaiveDateTime) -> String {
    dt.format("%H:%M").to_string()
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::NaiveDate;

    #[test]
    fn test_datetime_hm() {
        assert_eq!(datetime_hm(&NaiveDate::from_ymd(2019, 1, 1).and_hms(22, 21, 16)),
                   "22:21".to_string());
    }
}
