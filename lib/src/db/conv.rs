use chrono::{Datelike, Duration, NaiveDate, NaiveTime, Weekday};

use crate::result::*;

pub fn wildcarded(phrase: &str) -> String {
    format!("%{}%", phrase)
}

// TODO use integer for speed of comparisons
pub fn to_db_date(date: &NaiveDate) -> String {
    date.format("%Y%m%d").to_string()
}

pub fn to_db_time(local_time: &Duration) -> i64 {
    local_time.num_seconds()
}

pub fn naive_to_db_time(local_time: &NaiveTime) -> i64 {
    (*local_time - NaiveTime::from_hms(0, 0, 0)).num_seconds()
}

pub fn is_weekday(date: &NaiveDate, weekday: Weekday) -> i8 {
    if date.weekday() == weekday {
        1
    } else {
        0
    }
}

// FIXME: This is a crude way to be able to query `WHERE <something>
// in <array>`. Stock rusqlite doesn't support feeding an array as a
// value into a prepared statement. This should be supported by using
// the "array" extension, but at the moment the crate doesn't compile
// for me if i enable that extension.
// https://github.com/jgallagher/rusqlite/issues/430
pub fn sql_str_array(str_ary: &[&str]) -> ThprResult<String> {
    // Naive protection against SQL injection. I hate to do this
    // manually but the above issue leaves me with little choice.
    for str_elem in str_ary.iter() {
        if str_elem.contains("'") {
            return Err(ThprError::Generic(
                "Bad input to sql_str_array - quotes are not allowed".into()).into())
        }
    }

    Ok(format!("('{}')", str_ary.join("','")))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sql_str_array() {
        assert_eq!(sql_str_array(&["abc", "def", "ghi"]).expect("Failure in sql_str_array"),
                   "('abc','def','ghi')");
        assert!(sql_str_array(&["abc", "def'", "ghi"]).is_err());
    }
}
