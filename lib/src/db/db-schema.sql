CREATE TABLE agencies (
    agency_id TEXT NOT NULL PRIMARY KEY,
    agency_name TEXT NOT NULL,
    agency_timezone TEXT NOT NULL
);


CREATE TABLE stops (
    stop_id TEXT NOT NULL PRIMARY KEY,
    stop_name TEXT NOT NULL,
    stop_slug TEXT NOT NULL,
    wheelchair_boarding INTEGER
);

CREATE INDEX stops_by_name ON stops(stop_name);


CREATE TABLE routes (
    route_id TEXT NOT NULL PRIMARY KEY,
    agency_id TEXT NOT NULL,
    route_short_name TEXT NOT NULL,
    route_long_name TEXT NOT NULL,
    route_type INTEGER NOT NULL,

    FOREIGN KEY (agency_id) REFERENCES agencies(agency_id)
);

CREATE INDEX routes_by_agency ON routes(agency_id);


CREATE TABLE trips (
    route_id TEXT NOT NULL,
    service_id TEXT NOT NULL,
    trip_id TEXT NOT NULL PRIMARY KEY,
    direction_id INTEGER,
    wheelchair_accessible INTEGER,
    bikes_allowed INTEGER,

    FOREIGN KEY (route_id) REFERENCES routes(route_id) ON DELETE CASCADE
);

CREATE INDEX trips_by_route ON trips(route_id);
CREATE INDEX trips_by_service ON trips(service_id);


CREATE TABLE stop_times (
    trip_id TEXT NOT NULL,
    arrival_time INTEGER NOT NULL,
    departure_time INTEGER NOT NULL,
    stop_id TEXT NOT NULL,
    stop_sequence INTEGER NOT NULL,

    FOREIGN KEY (trip_id) REFERENCES trips(trip_id) ON DELETE CASCADE,
    FOREIGN KEY (stop_id) REFERENCES stops(stop_id)
);

CREATE INDEX stop_times_by_stop ON stop_times(stop_id);
CREATE INDEX stop_times_by_trip ON stop_times(trip_id);


CREATE TABLE calendars (
    service_id TEXT NOT NULL PRIMARY KEY,
    monday INTEGER NOT NULL,
    tuesday INTEGER NOT NULL,
    wednesday INTEGER NOT NULL,
    thursday INTEGER NOT NULL,
    friday INTEGER NOT NULL,
    saturday INTEGER NOT NULL,
    sunday INTEGER NOT NULL,
    start_date TEXT NOT NULL,
    end_date TEXT NOT NULL
);

CREATE INDEX calendars_by_service_id ON calendars(service_id);


CREATE TABLE calendar_dates (
    service_id TEXT NOT NULL,
    date TEXT NOT NULL,
    exception_type INTEGER NOT NULL
);

CREATE INDEX calendar_dates_by_service_id ON calendar_dates(service_id);
