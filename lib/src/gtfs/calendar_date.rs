use chrono::NaiveDate;
use std::path::Path;

use crate::{ThprResult, ThprError};
use super::new_reader_builder;
use super::serde::deser_gtfs_naive_date;

#[derive(Deserialize)]
pub struct CalendarDate {
    pub service_id: String,
    #[serde(deserialize_with="deser_gtfs_naive_date")]
    pub date: NaiveDate,
    pub exception_type: i32,
}

impl CalendarDate {
    pub fn from_csv(path: &Path) -> ThprResult<Vec<CalendarDate>> {
        let mut reader = new_reader_builder().from_path(path).
            map_err(|e| ThprError::Generic(
                format!("Can't open GTFS file {}: {}", path.to_string_lossy(), e)))?;
        let agencies: Result<Vec<CalendarDate>, _> = reader.deserialize().collect();
        Ok(agencies?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_csv() {
        let dates = &CalendarDate::from_csv(Path::new("../test-data/gtfs/CZ1_1/calendar_dates.txt"))
            .expect("Failed parsing calendar_dates.txt");
        assert_eq!(dates[0].service_id, "1");
        assert_eq!(dates[0].date, NaiveDate::from_ymd(2009, 9, 28));
        assert_eq!(dates[0].exception_type, 2);
    }
}
