use std::path::Path;

use crate::{ThprResult, ThprError};
use super::new_reader_builder;

#[derive(Deserialize)]
pub struct Route {
    pub route_id: String,
    pub agency_id: String,
    pub route_short_name: String,
    pub route_long_name: String,
    pub route_desc: Option<String>,
    pub route_type: i32,
    pub route_url: Option<String>,
    pub route_color: Option<String>,
    pub route_text_color: Option<String>,
    pub route_sort_order: Option<String>,
}

impl Route {
    pub fn from_csv(path: &Path) -> ThprResult<Vec<Route>> {
        let mut reader = new_reader_builder().from_path(path).
            map_err(|e| ThprError::Generic(
                format!("Can't open GTFS file {}: {}", path.to_string_lossy(), e)))?;
        let agencies: Result<Vec<Route>, _> = reader.deserialize().collect();
        Ok(agencies?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_csv() {
        let r = &Route::from_csv(Path::new("../test-data/gtfs/CZ1_1/routes.txt"))
            .expect("Failed parsing route.txt")[0];
        assert_eq!(r.route_id, "1");
        assert_eq!(r.route_short_name, "30");
        assert_eq!(r.route_type, 0);
    }
}
