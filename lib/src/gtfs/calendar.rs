use chrono::NaiveDate;
use std::path::Path;

use crate::{ThprResult, ThprError};
use super::new_reader_builder;
use super::serde::deser_gtfs_naive_date;

#[derive(Deserialize)]
pub struct Calendar {
    pub service_id: String,
    pub monday: i32,
    pub tuesday: i32,
    pub wednesday: i32,
    pub thursday: i32,
    pub friday: i32,
    pub saturday: i32,
    pub sunday: i32,
    #[serde(deserialize_with="deser_gtfs_naive_date")]
    pub start_date: NaiveDate,
    #[serde(deserialize_with="deser_gtfs_naive_date")]
    pub end_date: NaiveDate,
}

impl Calendar {
    pub fn from_csv(path: &Path) -> ThprResult<Vec<Calendar>> {
        let mut reader = new_reader_builder().from_path(path).
            map_err(|e| ThprError::Generic(
                format!("Can't open GTFS file {}: {}", path.to_string_lossy(), e)))?;
        let agencies: Result<Vec<Calendar>, _> = reader.deserialize().collect();
        Ok(agencies?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_csv() {
        let calendars = &Calendar::from_csv(Path::new("../test-data/gtfs/CZ1_1/calendar.txt"))
            .expect("Failed parsing calendar.txt");
        assert_eq!(calendars[0].service_id, "1");
        assert_eq!(calendars[0].monday, 1);
        assert_eq!(calendars[0].tuesday, 1);
        assert_eq!(calendars[0].saturday, 0);
        assert_eq!(calendars[0].sunday, 0);
        assert_eq!(calendars[0].start_date, NaiveDate::from_ymd(2009, 1, 1));
        assert_eq!(calendars[0].end_date, NaiveDate::from_ymd(2009, 12, 31));
    }
}
