use std::path::Path;
use chrono_tz::Tz;

use crate::{ThprResult, ThprError};
use super::new_reader_builder;
use super::serde::deser_gtfs_timezone;

#[derive(Deserialize)]
pub struct Agency {
    pub agency_id: String,
    pub agency_name: String,
    pub agency_url: String,
    #[serde(deserialize_with="deser_gtfs_timezone")]
    pub agency_timezone: Tz,
    pub agency_lang: Option<String>,
    pub agency_phone: Option<String>,
    pub agency_fare_url: Option<String>,
    pub agency_email: Option<String>,
}

impl Agency {
    pub fn from_csv(path: &Path) -> ThprResult<Vec<Agency>> {
        let mut reader = new_reader_builder().from_path(path).
            map_err(|e| ThprError::Generic(
                format!("Can't open GTFS file {}: {}", path.to_string_lossy(), e)))?;
        let agencies: Result<Vec<Agency>, _> = reader.deserialize().collect();
        Ok(agencies?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono_tz::Europe::Prague;

    #[test]
    fn test_from_csv() {
        let a = &Agency::from_csv(Path::new("../test-data/gtfs/CZ1_1/agency.txt"))
            .expect("Failed parsing agency.txt")[0];
        assert_eq!(a.agency_id, "CZ1_1");
        assert_eq!(a.agency_name, "Madeup Transport Company");
        assert_eq!(a.agency_url, "www.mtc.madeup");
        assert_eq!(a.agency_timezone, Prague);
        assert_eq!(a.agency_lang, None);
        assert_eq!(a.agency_phone, None);
        assert_eq!(a.agency_fare_url, None);
        assert_eq!(a.agency_email, None);
    }
}
