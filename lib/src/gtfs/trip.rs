use std::path::Path;

use crate::{ThprResult, ThprError};
use super::new_reader_builder;

#[derive(Deserialize)]
pub struct Trip {
    pub route_id: String,
    pub service_id: String,
    pub trip_id: String,
    pub trip_headsign: Option<String>,
    pub trip_short_name: Option<String>,
    pub direction_id: Option<i32>,
    pub block_id: Option<String>,
    pub shape_id: Option<String>,
    pub wheelchair_accessible: Option<i32>,
    pub bikes_allowed: Option<i32>,
}

impl Trip {
    pub fn from_csv(path: &Path) -> ThprResult<Vec<Trip>> {
        let mut reader = new_reader_builder().from_path(path).
            map_err(|e| ThprError::Generic(
                format!("Can't open GTFS file {}: {}", path.to_string_lossy(), e)))?;
        let agencies: Result<Vec<Trip>, _> = reader.deserialize().collect();
        Ok(agencies?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_csv() {
        let trips = &Trip::from_csv(Path::new("../test-data/gtfs/CZ1_1/trips.txt"))
            .expect("Failed parsing trips.txt");
        assert_eq!(trips[0].route_id, "1");
        assert_eq!(trips[0].service_id, "1");
        assert_eq!(trips[0].trip_id, "1");
        assert_eq!(trips[2].route_id, "1");
        assert_eq!(trips[2].service_id, "1");
        assert_eq!(trips[2].trip_id, "3");
    }
}
