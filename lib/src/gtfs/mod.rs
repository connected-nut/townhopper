use csv;
use std::path::Path;

use crate::ThprResult;

pub mod source;

mod serde;

mod agency;
mod calendar;
mod calendar_date;
mod route;
mod stop;
mod stop_time;
mod trip;

pub use self::agency::Agency;
pub use self::calendar::Calendar;
pub use self::calendar_date::CalendarDate;
pub use self::route::Route;
pub use self::stop::Stop;
pub use self::stop_time::StopTime;
pub use self::trip::Trip;

pub struct Gtfs {
    pub agencies: Vec<Agency>,
    pub calendars: Vec<Calendar>,
    pub calendar_dates: Vec<CalendarDate>,
    pub routes: Vec<Route>,
    pub stops: Vec<Stop>,
    pub stop_times: Vec<StopTime>,
    pub trips: Vec<Trip>,
}

impl Gtfs {
    pub fn from_dir(dir: &Path) -> ThprResult<Gtfs> {
        let agencies = Agency::from_csv(&dir.join("agency.txt"))?;
        let calendars = Calendar::from_csv(&dir.join("calendar.txt"))?;
        let calendar_dates = CalendarDate::from_csv(&dir.join("calendar_dates.txt"))?;
        let routes = Route::from_csv(&dir.join("routes.txt"))?;
        let stops = Stop::from_csv(&dir.join("stops.txt"))?;
        let stop_times = StopTime::from_csv(&dir.join("stop_times.txt"))?;
        let trips = Trip::from_csv(&dir.join("trips.txt"))?;
        Ok(Gtfs {
            agencies: agencies,
            calendars: calendars,
            calendar_dates: calendar_dates,
            routes: routes,
            stops: stops,
            stop_times: stop_times,
            trips: trips,
        })
    }
}

fn new_reader_builder() -> csv::ReaderBuilder {
    // Looks like the ReaderBuilder defaults are suitable, but we
    // still provide a single place to create the builders in case
    // something needs to change across all readers.
    csv::ReaderBuilder::new()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_dir() {
        Gtfs::from_dir(Path::new("../test-data/gtfs/CZ1_1"))
            .expect("Failed to load GTFS from directory");
    }
}
