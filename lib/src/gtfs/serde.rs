use chrono::{Duration, NaiveDate};
use chrono_tz::Tz;
use serde::{de, Deserialize, Deserializer};

use crate::result::*;

pub fn deser_gtfs_naive_date<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
where D: Deserializer<'de> {
    let raw = String::deserialize(deserializer)?;
    NaiveDate::parse_from_str(&raw, "%Y%m%d").map_err(de::Error::custom)
}

pub fn deser_gtfs_time<'de, D>(deserializer: D) -> Result<Duration, D::Error>
where D: Deserializer<'de> {
    let raw = String::deserialize(deserializer)?;
    let parts: Vec<&str> = raw.split(':').collect();
    if parts.len() != 3 {
        return Err(ThprError::Generic(format!("Could not parse GTFS time from '{}'", raw)))
            .map_err(de::Error::custom);
    }
    let ints = parts.iter().map(|part| part.parse().map_err(de::Error::custom))
        .collect::<Result<Vec<i64>, _>>()?;
    Ok(Duration::hours(ints[0]) + Duration::minutes(ints[1]) + Duration::seconds(ints[2]))
}

pub fn deser_gtfs_timezone<'de, D>(deserializer: D) -> Result<Tz, D::Error>
where D: Deserializer<'de> {
    let raw = String::deserialize(deserializer)?;
    Ok(raw.parse().map_err(de::Error::custom)?)
}

pub fn deser_gtfs_opt_timezone<'de, D>(deserializer: D) -> Result<Option<Tz>, D::Error>
where D: Deserializer<'de> {
    let raw = String::deserialize(deserializer)?;
    if raw.is_empty() {
        Ok(None)
    } else {
        Ok(Some(raw.parse().map_err(de::Error::custom)?))
    }
}
