use chrono::{NaiveDateTime, Duration, Weekday};
use rusqlite::Connection;
use std::collections::{HashSet, VecDeque};

use crate::db::conv;
use crate::model::*;
use crate::result::*;
use crate::query::stop::{only_stop_with_slug_part, load_stops};
use crate::query::route::load_routes;

const FIRST_DIRECT_TRIP_SQL: &'static str = include_str!("sql/first_direct_trip.sql");

pub fn generate_itineraries(conn: &Connection, query: &ItineraryQuery, count: u32)
                            -> ThprResult<Vec<Itinerary>> {
    let id_query = replace_stop_names_with_ids(conn, query)?;
    let mut departure_offset = Duration::zero();
    let mut itineraries: Vec<Itinerary> = vec![];
    for _ in 0..count {
        let mut offset_query = id_query.clone();
        offset_query.departure_time += departure_offset;
        let solver = ItinerarySolver::from_query(&offset_query)?;
        let itinerary = solver.solve(conn)?;
        departure_offset = itinerary.departure_time() - id_query.departure_time
            + Duration::seconds(1);

        itineraries.push(itinerary);
    }
    Ok(itineraries)
}

pub struct ItinerarySolver {
    pub current_time: NaiveDateTime,
    pub current_stop_id: String,
    pub elems: Vec<ItineraryElement>,
    pub query_elems: VecDeque<ItineraryQueryElement>,

    pub used_stop_ids: HashSet<String>,
    pub used_route_ids: HashSet<String>,
}

impl ItinerarySolver {
    pub fn from_query(query: &ItineraryQuery) -> ThprResult<Self> {
        let mut query_elem_iter = query.elements.clone().into_iter();
        let first_elem = query_elem_iter.next().ok_or(ThprError::BadQuery(
            format!("Cannot build ItinerarySolver: ItineraryQuery has no elements")))?;
        let from_stop_id = match first_elem {
            ItineraryQueryElement::FromStopId(id) => Ok(id),
            _ => Err(Box::new(
                ThprError::BadQuery("First element of ItineraryQuery must be FromStopId".into()))),
        }?;
        let mut used_stop_ids = HashSet::new();
        used_stop_ids.insert(from_stop_id.clone());
        Ok(Self {
            current_time: query.departure_time,
            current_stop_id: from_stop_id,
            elems: vec![],
            query_elems: query_elem_iter.collect(),
            used_stop_ids: used_stop_ids,
            used_route_ids: HashSet::new(),
        })
    }

    pub fn solve(mut self, conn: &Connection) -> ThprResult<Itinerary> {
        while let Some(qe) = self.query_elems.pop_front() {
            match qe {
                ItineraryQueryElement::FromStopId(ref id) => self.solve_from_stop_id(conn, id)?,
                ItineraryQueryElement::ToStopId(ref id) => self.solve_to_stop_id(conn, id)?,
                ItineraryQueryElement::Gap(ref duration) => self.solve_gap(conn, duration)?,
                _ => Err(ThprError::Generic("not implemented".into()))?,
            }
        }

        Ok(Itinerary {
            elements: self.elems,
            stop_data: load_stops(conn, &self.used_stop_ids.iter().map(AsRef::as_ref)
                                  .collect::<Vec<&str>>())?,
            route_data: load_routes(conn, &self.used_route_ids.iter().map(AsRef::as_ref)
                                    .collect::<Vec<&str>>())?,
        })
    }

    fn solve_from_stop_id(&mut self, _conn: &Connection, stop_id: &str) -> ThprResult<()> {
        self.used_stop_ids.insert(stop_id.to_string());
        self.current_stop_id = stop_id.to_string();
        Ok(())
    }

    fn solve_to_stop_id(&mut self, conn: &Connection, stop_id: &str) -> ThprResult<()> {
        let board_data = first_direct_trip(
            conn, &self.current_time, &self.current_stop_id, stop_id)?;
        self.current_time = board_data.arrival_time.clone();
        self.current_stop_id = board_data.arrival_stop_id.clone();
        self.used_stop_ids.insert(stop_id.to_string());
        self.used_route_ids.insert(board_data.route_id.clone());
        self.elems.push(ItineraryElement::BoardVehicle(board_data));
        Ok(())
    }

    fn solve_gap(&mut self, _conn: &Connection, duration: &Duration) -> ThprResult<()> {
        self.current_time += *duration;
        Ok(())
    }
}

fn replace_stop_names_with_ids(conn: &Connection, query: &ItineraryQuery)
                               -> ThprResult<ItineraryQuery> {
    let mut replaced = (*query).clone();
    let old_elems = replaced.elements;
    replaced.elements = vec![];

    // cycle is a better fit than map() because the inner operation
    // needs to bail out on failure
    for elem in old_elems.into_iter() {
        replaced.elements.push(match elem {
            ItineraryQueryElement::FromStop(ref stop) => ItineraryQueryElement::FromStopId(
                only_stop_with_slug_part(conn, stop)?.id),
            ItineraryQueryElement::ToStop(ref stop) => ItineraryQueryElement::ToStopId(
                only_stop_with_slug_part(conn, &stop)?.id),
            anything => anything,
        });
    };

    Ok(replaced)
}

fn first_direct_trip(conn: &Connection, dt: &NaiveDateTime, from_stop_id: &str, to_stop_id: &str)
                     -> ThprResult<BoardVehicleData> {
    let mut stmt = conn.prepare(FIRST_DIRECT_TRIP_SQL)?;
    let mut rows = stmt.query_named(&[
        (":from_stop_id", &from_stop_id),
        (":to_stop_id", &to_stop_id),
        (":departure_date", &conv::to_db_date(&dt.date())),
        (":departure_time", &conv::naive_to_db_time(&dt.time())),
        (":monday", &conv::is_weekday(&dt.date(), Weekday::Mon)),
        (":tuesday", &conv::is_weekday(&dt.date(), Weekday::Tue)),
        (":wednesday", &conv::is_weekday(&dt.date(), Weekday::Wed)),
        (":thursday", &conv::is_weekday(&dt.date(), Weekday::Thu)),
        (":friday", &conv::is_weekday(&dt.date(), Weekday::Fri)),
        (":saturday", &conv::is_weekday(&dt.date(), Weekday::Sat)),
        (":sunday", &conv::is_weekday(&dt.date(), Weekday::Sun)),
    ])?;
    if let Some(row) = rows.next()? {
        let route_id: String = row.get(0)?;
        let trip_id: String = row.get(1)?;
        let from_stop_id: String = row.get(2)?;
        let to_stop_id: String = row.get(3)?;
        let departure_time: i64 = row.get(4)?;
        let arrival_time: i64 = row.get(5)?;
        Ok(BoardVehicleData {
            route_id: route_id.clone(),
            trip_id: trip_id.clone(),
            departure_stop_id: from_stop_id,
            departure_time: dt.date().and_hms(0, 0, 0) + Duration::seconds(departure_time),
            arrival_stop_id: to_stop_id,
            arrival_time: dt.date().and_hms(0, 0, 0) + Duration::seconds(arrival_time),
        })
    } else {
        // FIXME: handle this nicely, probably Option rather than Result
        Err("not found".into())
    }
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;

    use super::*;
    use crate::model::ItineraryQueryBuilder;
    use crate::util::test::db_w_data;

    #[test]
    fn test_replace_stop_names_with_ids() {
        let conn = db_w_data();
        let iq_w_names = ItineraryQueryBuilder::new()
            .add_stop("sometown-third")
            .add_stop("sometown-tenth")
            .build();
        let iq_w_ids = replace_stop_names_with_ids(&conn, &iq_w_names)
            .expect("Failed to replace stop names with IDs");
        assert_eq!(iq_w_ids.departure_time, iq_w_names.departure_time);
        assert_eq!(iq_w_ids.elements[0], ItineraryQueryElement::FromStopId("3".into()));
        assert_eq!(iq_w_ids.elements[1], ItineraryQueryElement::ToStopId("10".into()));
    }

    #[test]
    fn test_generate_itineraries_simple() {
        let conn = db_w_data();
        let iq = ItineraryQueryBuilder::new()
            .set_departure_time(&NaiveDate::from_ymd(2009, 4, 14).and_hms(8, 0, 0))
            .add_stop("sixth")
            .add_stop("tenth")
            .build();
        let i = generate_itineraries(&conn, &iq, 1).expect("Failed to generate itineraries");
        assert_eq!(i[0].elements.len(), 1);
        if let ItineraryElement::BoardVehicle(ref data) = i[0].elements[0] {
            assert_eq!(data.departure_stop_id, "6".to_string());
            assert_eq!(data.departure_time,
                       NaiveDate::from_ymd(2009, 4, 14).and_hms(8, 11, 0));
            assert_eq!(data.arrival_stop_id, "10".to_string());
            assert_eq!(data.arrival_time,
                       NaiveDate::from_ymd(2009, 4, 14).and_hms(8, 17, 0));
        } else {
            panic!("Did not find vehicle boarding itinerary element")
        }
    }
}
