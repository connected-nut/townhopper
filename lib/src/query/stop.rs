use rusqlite::Connection;
use std::collections::HashMap;

use crate::db::conv::{wildcarded, sql_str_array};
use crate::model::Stop;
use crate::result::*;

pub fn stops_with_slug_part(conn: &Connection, slug_part: &str) -> ThprResult<Vec<Stop>> {
    let mut prep_stmt = conn.prepare("SELECT stop_id, stop_name, stop_slug
        FROM stops
        WHERE stop_slug LIKE ?
        ORDER BY stop_slug")?;
    let mut rows = prep_stmt.query(&[&wildcarded(slug_part)])?;
    let mut stops: Vec<Stop> = vec![];
    while let Some(row) = rows.next()? {
        stops.push(Stop::from_pos_row(&row)?);
    }
    Ok(stops)
}

pub fn only_stop_with_slug_part(conn: &Connection, slug_part: &str) -> ThprResult<Stop> {
    let stops = stops_with_slug_part(conn, slug_part)?;
    match stops.len() {
        0 => Err(ThprError::BadQuery(
            format!("No matching stop found for '{}'", slug_part)).into()),
        1 => Ok(stops.into_iter().next().expect("Error when iterating over stops")),
        _ => {
            for stop in stops.into_iter() {
                if stop.slug == slug_part {
                    return Ok(stop)
                }
            }
            Err(ThprError::BadQuery(
                format!("Multiple matching stops found for '{}'", slug_part)).into())
        }
    }
}

pub fn load_stops(conn: &Connection, stop_ids: &[&str]) -> ThprResult<HashMap<String, Stop>> {
    let mut stmt = conn.prepare(&format!(
        "SELECT stop_id, stop_name, stop_slug
        FROM stops
        WHERE stop_id IN {}",
        sql_str_array(stop_ids)?))?;
    let mut rows = stmt.query(rusqlite::NO_PARAMS)?;
    let mut stops = HashMap::new();
    while let Some(row) = rows.next()? {
        let stop = Stop::from_pos_row(&row)?;
        stops.insert(stop.id.clone(), stop);
    }
    Ok(stops)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::test::db_w_data;

    #[test]
    fn test_stops_with_slug_part() {
        let conn = db_w_data();
        let stops = stops_with_slug_part(&conn, "six").expect("Error when searching for stops");
        assert_eq!(stops.len(), 2);
        assert_eq!(stops[0].name, "Sometown, Sixteenth");
        assert_eq!(stops[1].name, "Sometown, Sixth");
    }

    #[test]
    fn test_only_stop_with_slug_part() {
        let conn = db_w_data();
        assert_eq!(
            only_stop_with_slug_part(&conn, "sixteen")
                .expect("Error when searching for stops")
                .name,
            "Sometown, Sixteenth");
        assert!(only_stop_with_slug_part(&conn, "fourty-fourth").is_err());
        assert!(only_stop_with_slug_part(&conn, "six").is_err());
        // use exact match when duplicates found
        assert_eq!(
            only_stop_with_slug_part(&conn, "sometown-twenty-third")
                .expect("Error when searching for stops")
                .name,
            "Sometown, Twenty-third");
    }
}
