use chrono::{Duration, NaiveDateTime};
use std::collections::HashMap;

use crate::model::{Route, Stop};

#[derive(Debug)]
pub struct Itinerary {
    pub elements: Vec<ItineraryElement>,
    pub route_data: HashMap<String, Route>,
    pub stop_data: HashMap<String, Stop>,
}

#[derive(Debug)]
pub enum ItineraryElement {
    BoardVehicle(BoardVehicleData),
    Wait(Duration),
}

#[derive(Debug)]
pub struct BoardVehicleData {
    pub route_id: String,
    pub trip_id: String,
    pub departure_stop_id: String,
    pub departure_time: NaiveDateTime,
    pub arrival_stop_id: String,
    pub arrival_time: NaiveDateTime,
}

impl Itinerary {
    pub fn departure_time(&self) -> NaiveDateTime {
        for elem in self.elements.iter() {
            match elem {
                ItineraryElement::BoardVehicle(ref bvd) => return bvd.departure_time,
                _ => {},
            }
        }
        panic!("Itinerary departure time not found - itinerary had no BoardVehicle element!");
    }
}
