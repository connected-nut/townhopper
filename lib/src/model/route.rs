use rusqlite::Row;

use crate::result::*;

#[derive(Debug)]
pub struct Route {
    pub id: String,
    pub short_name: String,
}

impl Route {
    pub fn from_pos_row(r: &Row) -> ThprResult<Self> {
        Ok(Self {
            id: r.get::<usize, Option<String>>(0)?.ok_or("Error fetching stop ID")?,
            short_name: r.get::<usize, Option<String>>(1)?.ok_or("Error fetching stop name")?,
        })
    }
}
