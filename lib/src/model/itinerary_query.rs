use chrono::{Duration, Local, NaiveDateTime};

#[derive(Clone, Debug, PartialEq)]
pub struct ItineraryQuery {
    pub departure_time: NaiveDateTime,
    pub elements: Vec<ItineraryQueryElement>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum ItineraryQueryElement {
    /// Resets trip planner to perform the next query as travelling
    /// from this stop. Useful when a transfer includes moving to a
    /// different stop.
    FromStop(String),
    /// Same as FromStop, but an actual stop ID rather than a
    /// user-specified name or slug.
    FromStopId(String),
    /// Minimal time gap to reserve for transfer.
    Gap(Duration),
    /// Next stop.
    ToStop(String),
    /// Same as ToStop, but an actual stop ID rather than a
    /// user-specified name or slug.
    ToStopId(String),
}
impl ItineraryQueryElement {
    pub fn new_gap_minutes(minutes: i64) -> Self {
        ItineraryQueryElement::Gap(Duration::minutes(minutes))
    }
}

#[derive(Debug)]
pub struct ItineraryQueryBuilder {
    wip_query: ItineraryQuery,
}

impl ItineraryQueryBuilder {
    pub fn new() -> Self {
        Self {
            wip_query: ItineraryQuery {
                departure_time: Local::now().naive_local(),
                elements: vec![],
            },
        }
    }

    pub fn set_departure_time(self, time: &NaiveDateTime) -> Self {
        let mut wip_query = self.wip_query;
        wip_query.departure_time = time.clone();

        Self {
            wip_query: wip_query,
        }
    }

    pub fn add_stop(self, stop: &str) -> Self {
        self.add_element(ItineraryQueryElement::ToStop(stop.into()))
    }

    pub fn add_from_stop(self, stop: &str) -> Self {
        self.add_element(ItineraryQueryElement::FromStop(stop.into()))
    }

    pub fn add_gap(self, minutes: i64) -> Self {
        self.add_element(ItineraryQueryElement::new_gap_minutes(minutes))
    }

    pub fn add_element(self, iqe: ItineraryQueryElement) -> Self {
        let mut wip_query = self.wip_query;
        if wip_query.elements.is_empty()  {
            match iqe {
                ItineraryQueryElement::ToStop(stop) =>
                    wip_query.elements.push(ItineraryQueryElement::FromStop(stop.into())),
                _ => wip_query.elements.push(iqe),
            }
        } else {
            wip_query.elements.push(iqe);
        }

        Self {
            wip_query: wip_query,
        }
    }

    pub fn build(self) -> ItineraryQuery {
        self.wip_query
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_itinerary_query_builder() {
        let iq = ItineraryQueryBuilder::new()
            .set_departure_time(&NaiveDateTime::from_timestamp(1546185340, 0))
            .add_stop("third-stop")
            .add_stop("tenth-stop")
            .add_gap(5)
            .add_from_stop("eleventh-stop")
            .add_stop("twentieth-stop")
            .build();
        assert_eq!(iq.elements[0], ItineraryQueryElement::FromStop("third-stop".into()));
        assert_eq!(iq.elements[1], ItineraryQueryElement::ToStop("tenth-stop".into()));
        assert_eq!(iq.elements[2], ItineraryQueryElement::Gap(Duration::minutes(5)));
        assert_eq!(iq.elements[3], ItineraryQueryElement::FromStop("eleventh-stop".into()));
        assert_eq!(iq.elements[4], ItineraryQueryElement::ToStop("twentieth-stop".into()));
    }
}
