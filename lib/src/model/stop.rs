use rusqlite::Row;

use crate::result::*;

#[derive(Debug)]
pub struct Stop {
    pub id: String,
    pub name: String,
    pub slug: String,
}

impl Stop {
    pub fn from_pos_row(r: &Row) -> ThprResult<Self> {
        Ok(Stop {
            id: r.get::<usize, Option<String>>(0)?.ok_or("Error fetching stop ID")?,
            name: r.get::<usize, Option<String>>(1)?.ok_or("Error fetching stop name")?,
            slug: r.get::<usize, Option<String>>(2)?.ok_or("Error fetching stop slug")?,
        })
    }
}
