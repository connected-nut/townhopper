Townhopper
==========

CLI program for simple queries on GTFS public transport timetables.

Installation
------------

    cargo install townhopper_cli

Configuring
-----------

Create a config file at `~/.config/townhopper/config.toml` and point
to a directory with your GTFS data files, somewhat like:

    [[gtfs_sources]]
    kind = "dir_of_dirs"
    path = "../../.local/share/townhopper/gtfs"

The above config assumes that under `~/.local/share/townhopper/gtfs`
there is at least one subdirectory containing GTFS `txt` files.

Run a refresh command which will read the GTFS data and populate
townhopper's internal SQLite database:

    $ townhopper refresh

Usage
-----

More comprehensive guide or a proper manual page should be added once
the program becomes more usable, but here's an example how to query
the nearest connection in Brno from Hlavní nádraží to Česká with a
transfer at Mendlovo náměstí:

    $ townhopper trip -s brno-hlavni-nadrazi -s brno-mendlovo-namesti -s brno-ceska
    Departure: 2019-03-02 19:04

                  1 >> 19:12 Brno, Hlavní nádraží
                    << 19:18 Brno, Mendlovo náměstí
                  6 >> 19:18 Brno, Mendlovo náměstí
                    << 19:25 Brno, Česká

                  1 >> 19:22 Brno, Hlavní nádraží
                    << 19:28 Brno, Mendlovo náměstí
                  6 >> 19:28 Brno, Mendlovo náměstí
                    << 19:35 Brno, Česká

                  1 >> 19:32 Brno, Hlavní nádraží
                    << 19:38 Brno, Mendlovo náměstí
                  6 >> 19:38 Brno, Mendlovo náměstí
                    << 19:45 Brno, Česká

For a description of current subcommands and features, use help:

    $ townhopper -h
    $ townhopper trip -h

Development
-----------

To build both the library and CLI on the host:

    make

Likely it's better to build in a container:

    ./toolbox/run make

If you're hacking both on lib and CLI at the same time, make sure the
CLI will use the local lib (not the one from crates.io) by
uncommenting the following in CLI's Cargo.toml:

    [patch.crates-io]
    townhopper = { path = "../lib" }
