#!/bin/bash

set -euxo pipefail

DIR=$(cd $(dirname "${BASH_SOURCE[0]}") &> /dev/null; pwd)
CLI_DIR=${CLI_DIR:-$DIR/..}
TEST_DATA_DIR=${TEST_DATA_DIR:-$DIR/../../test-data}

THPR_BIN="$CLI_DIR"/target/debug/townhopper
CONFIG_FILE="$CLI_DIR"/test-func/config.toml
CACHE_HOME="$CLI_DIR/test-func/cache"

rm -r "$CACHE_HOME" || true
mkdir "$CACHE_HOME"
export XDG_CACHE_HOME="$CACHE_HOME"

function test_refresh() {
    "$THPR_BIN" -C "$CONFIG_FILE" refresh
}

function test_search_stop() {
    SEARCH_STOP_RESULT=$("$THPR_BIN" -C "$CONFIG_FILE" search stop eight)
    grep Eighth <<<"$SEARCH_STOP_RESULT"
    grep Eighteenth <<<"$SEARCH_STOP_RESULT"
    grep -v First <<<"$SEARCH_STOP_RESULT"
}

function test_trip() {
    TRIP_RESULT=$("$THPR_BIN" -C "$CONFIG_FILE" trip \
                              -c 2 \
                              -d '2009-04-08 08:00' \
                              -s sometown-sixth \
                              -s sometown-seventh \
                              -w 3 \
                              -s sometown-sixteenth)
    grep Sixth <<<"$TRIP_RESULT" | grep 8:11
    grep Seventh <<<"$TRIP_RESULT" | grep 8:12
    grep Seventh <<<"$TRIP_RESULT" | grep 8:27
    grep Sixteenth <<<"$TRIP_RESULT" | grep 8:41
    grep Sixth <<<"$TRIP_RESULT" | grep 8:26
    grep Seventh <<<"$TRIP_RESULT" | grep 8:27
    grep Seventh <<<"$TRIP_RESULT" | grep 8:42
    grep Sixteenth <<<"$TRIP_RESULT" | grep 8:56
}

test_refresh
test_search_stop
test_trip
