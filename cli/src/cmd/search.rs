use clap::{App, AppSettings, Arg, SubCommand, ArgMatches};

use crate::ThprCliResult;
use crate::db;
use townhopper::query;

pub fn create_subcommand<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("search")
        .about("Search for items in the database")
        .setting(AppSettings::SubcommandRequired)
        .subcommand(
            SubCommand::with_name("stop")
                .about("Search for stops in the database")
                .arg(Arg::with_name("query")
                     .value_name("QUERY")
                     .required(true)
                     .takes_value(true)))
}

pub fn run(_top_matches: &ArgMatches, sub_matches: &ArgMatches) -> ThprCliResult<()> {
    match sub_matches.subcommand() {
        ("stop", Some(sub_sub_matches)) => search_stop(&sub_sub_matches),
        _ => { Err("Unknown search subcommand.".into()) },
    }
}

pub fn search_stop(stop_matches: &ArgMatches) -> ThprCliResult<()> {
    let dbl = db::lease()?;
    let conn = dbl.as_conn();

    db::begin(conn)?;
    let stops = query::stop::stops_with_slug_part(
        conn,
        stop_matches.value_of("query").expect("Argument 'query' not provided"),
    )?;
    db::end(conn)?;
    for stop in stops.into_iter() {
        println!("{}   ({})", stop.name, stop.slug);
    }
    Ok(())
}
