use chrono::Local;
use chrono_systemd_time::parse_timestamp_tz;
use clap::{App, Arg, SubCommand, ArgMatches};

use crate::db;
use crate::result::*;
use townhopper::model::*;
use townhopper::query::itinerary;
use townhopper::util::fmt::datetime_hm;

pub fn create_subcommand<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("trip")
        .about("Plan a trip itinerary")
        .arg(Arg::with_name("count")
             .help("Number of itineraries to generate")
             .short("c")
             .long("count")
             .default_value("3")
             .takes_value(true))
        .arg(Arg::with_name("depart")
             .help("Date and/or time of departure")
             .short("d")
             .long("depart")
             .takes_value(true))
        .arg(Arg::with_name("from")
             .help("Stop to travel from")
             .short("f")
             .long("from")
             .multiple(true)
             .takes_value(true))
        .arg(Arg::with_name("stop")
             .help("Stop to travel to")
             .short("s")
             .long("stop")
             .multiple(true)
             .required(true)
             .takes_value(true))
        .arg(Arg::with_name("wait")
             .help("Minimal time gap for transfer")
             .short("w")
             .long("wait")
             .multiple(true)
             .takes_value(true))
}

pub fn run(_top_matches: &ArgMatches, sub_matches: &ArgMatches) -> ThprCliResult<()> {
    let dbl = db::lease()?;
    let iq = build_itinerary_query(&sub_matches)?;
    let count = sub_matches.value_of("count")
        .expect("Parameter '--count' must be provided")
        .parse::<u32>().map_err(|e| format!("{}", e))?
        .max(1).min(10);
    print_itinerary_query(&iq)?;
    let itineraries = itinerary::generate_itineraries(dbl.as_conn(), &iq, count)?;
    for itinerary in itineraries.iter() {
        print_itinerary(itinerary)?;
    }
    Ok(())
}

fn build_itinerary_query(sub_matches: &ArgMatches) -> ThprCliResult<ItineraryQuery> {
    let mut builder = ItineraryQueryBuilder::new();

    if let Some(depart_str) = sub_matches.value_of("depart") {
        builder = builder.set_departure_time(&parse_timestamp_tz(depart_str, Local)
            .map_err(|e| ThprCliError::Generic(format!("{}", e)))?
            .naive_local());
    }

    // Vec of (element position, element). Eventually should get
    // sorted by element position and processed in order.
    let mut iqes: Vec<(usize, ItineraryQueryElement)> = vec![];

    // '--from' might be omitted completely
    let from_vals_vec: Vec<&str> = sub_matches.values_of("from")
        .map(|iter| iter.collect()).unwrap_or(vec![]);
    let mut from_vals = from_vals_vec.iter();
    let from_indices_vec: Vec<usize> = sub_matches.indices_of("from")
        .map(|iter| iter.collect()).unwrap_or(vec![]);
    for idx in from_indices_vec.into_iter() {
        iqes.push(
            (idx,
             ItineraryQueryElement::FromStop(
                 from_vals.next().expect("Unexpected missing 'from' arg value").to_string())));
    }

    // '--wait' might be omitted completely
    let wait_vals_vec: Vec<&str> = sub_matches.values_of("wait")
        .map(|iter| iter.collect()).unwrap_or(vec![]);
    let mut wait_vals = wait_vals_vec.iter();
    let wait_indices_vec: Vec<usize> = sub_matches.indices_of("wait")
        .map(|iter| iter.collect()).unwrap_or(vec![]);
    for idx in wait_indices_vec.into_iter() {
        let wait_str = wait_vals.next().expect("Unexpected missing 'wait' arg value").to_string();
        iqes.push(
            (idx,
             ItineraryQueryElement::new_gap_minutes(
                 wait_str.parse().expect(&format!(
                     "Unable to 'wait' argument value ('{}') as integer", wait_str)))));
    }

    let mut stop_vals = sub_matches.values_of("stop")
        .expect("At least one '--stop' must be provided");
    let stop_indices = sub_matches.indices_of("stop")
        .expect("At least one '--stop' must be provided");
    for idx in stop_indices {
        iqes.push(
            (idx,
             ItineraryQueryElement::ToStop(
                 stop_vals.next().expect("Unexpected missing 'stop' arg value").to_string())));
    }

    iqes.sort_by(|a, b| {
        let (idx_a, _) = a;
        let (idx_b, _) = b;
        idx_a.cmp(idx_b)
    });

    for (_, iqe) in iqes.into_iter() {
        builder = builder.add_element(iqe);
    }
    Ok(builder.build())
}

fn print_itinerary_query(iq: &ItineraryQuery) -> ThprCliResult<()> {
    println!("Departure: {}", iq.departure_time.format("%Y-%m-%d %H:%M"));
    Ok(())
}

fn print_itinerary(itinerary: &Itinerary) -> ThprCliResult<()> {
    println!();
    for elem in itinerary.elements.iter() {
        match elem {
            ItineraryElement::BoardVehicle(ref bvd) => print_board_vehicle(itinerary, bvd)?,
            _ => {},
        }
    }
    Ok(())
}

fn print_board_vehicle(itinerary: &Itinerary, bvd: &BoardVehicleData) -> ThprCliResult<()> {
    let departure_stop_name = &data_lookup(&itinerary.stop_data, &bvd.departure_stop_id)?.name;
    let arrival_stop_name = &data_lookup(&itinerary.stop_data, &bvd.arrival_stop_id)?.name;
    let route_name = &data_lookup(&itinerary.route_data, &bvd.route_id)?.short_name;

    println!("{:>15} >> {} {}",
             &route_name,
             &datetime_hm(&bvd.departure_time), departure_stop_name);
    println!("                << {} {}",
             &datetime_hm(&bvd.arrival_time), arrival_stop_name);
    Ok(())
}
