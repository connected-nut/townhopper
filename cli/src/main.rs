#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;

extern crate chrono;
extern crate chrono_systemd_time;
extern crate clap;
extern crate config;
extern crate rusqlite;
extern crate stderrlog;
extern crate townhopper;
extern crate xdg;

pub mod db;
pub mod cfg;
pub mod cmd;
pub mod result;

use clap::{App, Arg, ArgMatches};
use std::error::Error;
use std::path::{Path, PathBuf};
use std::process::exit;

pub use crate::result::{ThprCliResult, ThprCliError};

fn main() {
    match main_result() {
        Ok(_) => {},
        Err(e) => { eprintln!("Error: {}", e); exit(1) },
    };
}

fn main_result() -> Result<(), Box<dyn Error>> {
    let mut app = App::new("Townhopper")
        .about("Transport timetable query tool")
        .arg(Arg::with_name("verbose")
             .short("v")
             .multiple(true)
             .help("Increase message verbosity, can be used multiple times"))
        .arg(Arg::with_name("config")
             .short("C")
             .takes_value(true)
             .help("Customize config file location"))
        .subcommand(cmd::refresh::create_subcommand())
        .subcommand(cmd::search::create_subcommand())
        .subcommand(cmd::trip::create_subcommand());
    let matches = app.clone().get_matches();

    if matches.subcommand_name().is_none() {
        app.print_long_help().expect("Error when printing help");
        exit(1);
    }

    let verbosity = matches.occurrences_of("verbose") as usize;
    // 0 => LevelFilter::Error,
    // 1 => LevelFilter::Warn,
    // 2 => LevelFilter::Info,
    // 3 => LevelFilter::Debug,
    // _ => LevelFilter::Trace,
    stderrlog::new()
        .verbosity(verbosity + 1)
        .timestamp(stderrlog::Timestamp::Millisecond)
        .init()
        .expect("Failed to initialize logging");

    load_config(&matches)?;
    db::connect()?;

    match matches.subcommand() {
        ("refresh", Some(sub_matches)) => cmd::refresh::run(&matches, &sub_matches),
        ("search", Some(sub_matches)) => cmd::search::run(&matches, &sub_matches),
        ("trip", Some(sub_matches)) => cmd::trip::run(&matches, &sub_matches),
        _ => { Err(ThprCliError::generic("Unknown subcommand.")) },
    }.map_err(|e| e.into())
}

fn load_config(matches: &ArgMatches) -> Result<(), Box<dyn Error>> {
    cfg::init();
    let config_path_opt: Option<PathBuf> = match matches.value_of("config") {
        Some(path_str) => Some(Path::new(path_str).to_path_buf()),
        None => cfg::find_config(),
    };
    if let Some(ref config_path) = config_path_opt {
        debug!("Loading config file '{}'", &config_path.to_string_lossy());
        cfg::load_config(config_path).expect("Failure when loading config file");
    } else {
        error!("Config file not found. Create one in $XDG_CONFIG_HOME/townhopper/config.toml. \
                (Usually ~/.config/townhopper/config.toml.)");
        exit(1);
    }
    cfg::validate()?;
    Ok(())
}
